<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\Models\Directivos */

$this->title = 'Update Directivos: ' . $model->dni;
$this->params['breadcrumbs'][] = ['label' => 'Directivos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->dni, 'url' => ['view', 'id' => $model->dni]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="directivos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
