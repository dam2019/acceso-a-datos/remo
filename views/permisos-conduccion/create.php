<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\Models\PermisosConduccion */

$this->title = 'Create Permisos Conduccion';
$this->params['breadcrumbs'][] = ['label' => 'Permisos Conduccions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="permisos-conduccion-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
