<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Permisos Conduccions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="permisos-conduccion-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Permisos Conduccion', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_permiso',
            'dni_entrenador',
            'permiso',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
