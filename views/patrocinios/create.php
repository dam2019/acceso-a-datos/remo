<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\Models\Patrocinios */

$this->title = 'Create Patrocinios';
$this->params['breadcrumbs'][] = ['label' => 'Patrocinios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="patrocinios-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
