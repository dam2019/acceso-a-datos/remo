<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Models\UsosEmbarcaciones */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="usos-embarcaciones-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'matricula_embarcacion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'id_remero')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
