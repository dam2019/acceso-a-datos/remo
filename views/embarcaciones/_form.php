<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Models\Embarcaciones */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="embarcaciones-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'matricula')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nombre_tecnico')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'mote')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fabricante')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'num_tripulantes')->textInput() ?>

    <?= $form->field($model, 'necesita_patron')->textInput() ?>

    <?= $form->field($model, 'eslora')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
