<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\Models\Embarcaciones */

$this->title = 'Update Embarcaciones: ' . $model->matricula;
$this->params['breadcrumbs'][] = ['label' => 'Embarcaciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->matricula, 'url' => ['view', 'id' => $model->matricula]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="embarcaciones-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
