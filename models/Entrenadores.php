<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "entrenadores".
 *
 * @property string $dni
 * @property string $nombre_completo
 * @property string|null $titulacion
 * @property string $dni_directivo
 * @property string $codigo_categoria
 *
 * @property Categorias $codigoCategoria
 * @property Directivos $dniDirectivo
 * @property PermisosConduccion[] $permisosConduccions
 */
class Entrenadores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'entrenadores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dni', 'nombre_completo', 'dni_directivo', 'codigo_categoria'], 'required'],
            [['dni', 'dni_directivo'], 'string', 'max' => 9],
            [['nombre_completo', 'titulacion'], 'string', 'max' => 30],
            [['codigo_categoria'], 'string', 'max' => 2],
            [['dni'], 'unique'],
            [['codigo_categoria'], 'exist', 'skipOnError' => true, 'targetClass' => Categorias::className(), 'targetAttribute' => ['codigo_categoria' => 'codigo']],
            [['dni_directivo'], 'exist', 'skipOnError' => true, 'targetClass' => Directivos::className(), 'targetAttribute' => ['dni_directivo' => 'dni']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'dni' => 'Dni',
            'nombre_completo' => 'Nombre Completo',
            'titulacion' => 'Titulacion',
            'dni_directivo' => 'Dni Directivo',
            'codigo_categoria' => 'Codigo Categoria',
        ];
    }

    /**
     * Gets query for [[CodigoCategoria]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoCategoria()
    {
        return $this->hasOne(Categorias::className(), ['codigo' => 'codigo_categoria']);
    }

    /**
     * Gets query for [[DniDirectivo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDniDirectivo()
    {
        return $this->hasOne(Directivos::className(), ['dni' => 'dni_directivo']);
    }

    /**
     * Gets query for [[PermisosConduccions]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPermisosConduccions()
    {
        return $this->hasMany(PermisosConduccion::className(), ['dni_entrenador' => 'dni']);
    }
}
