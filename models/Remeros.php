<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "remeros".
 *
 * @property int $id_remero
 * @property string|null $codigo_categoria
 * @property int|null $codigo_remero
 * @property string $dni
 * @property string $nombre_completo
 * @property string|null $fecha_nac
 * @property int|null $codigo_patrocinador
 * @property string|null $lesiones
 * @property int|null $anios_exp
 * @property string|null $datos_padre
 *
 * @property Categorias $codigoCategoria
 * @property Patrocinadores $codigoPatrocinador
 * @property UsosEmbarcaciones[] $usosEmbarcaciones
 * @property Embarcaciones[] $matriculaEmbarcacions
 */
class Remeros extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'remeros';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_remero', 'codigo_patrocinador', 'anios_exp'], 'integer'],
            [['dni', 'nombre_completo'], 'required'],
            [['fecha_nac'], 'safe'],
            [['codigo_categoria'], 'string', 'max' => 2],
            [['dni'], 'string', 'max' => 9],
            [['nombre_completo'], 'string', 'max' => 30],
            [['lesiones', 'datos_padre'], 'string', 'max' => 50],
            [['dni'], 'unique'],
            [['codigo_categoria', 'codigo_remero'], 'unique', 'targetAttribute' => ['codigo_categoria', 'codigo_remero']],
            [['codigo_categoria'], 'exist', 'skipOnError' => true, 'targetClass' => Categorias::className(), 'targetAttribute' => ['codigo_categoria' => 'codigo']],
            [['codigo_patrocinador'], 'exist', 'skipOnError' => true, 'targetClass' => Patrocinadores::className(), 'targetAttribute' => ['codigo_patrocinador' => 'codigo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_remero' => 'Id Remero',
            'codigo_categoria' => 'Codigo Categoria',
            'codigo_remero' => 'Codigo Remero',
            'dni' => 'Dni',
            'nombre_completo' => 'Nombre Completo',
            'fecha_nac' => 'Fecha Nac',
            'codigo_patrocinador' => 'Codigo Patrocinador',
            'lesiones' => 'Lesiones',
            'anios_exp' => 'Anios Exp',
            'datos_padre' => 'Datos Padre',
        ];
    }

    /**
     * Gets query for [[CodigoCategoria]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoCategoria()
    {
        return $this->hasOne(Categorias::className(), ['codigo' => 'codigo_categoria']);
    }

    /**
     * Gets query for [[CodigoPatrocinador]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoPatrocinador()
    {
        return $this->hasOne(Patrocinadores::className(), ['codigo' => 'codigo_patrocinador']);
    }

    /**
     * Gets query for [[UsosEmbarcaciones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsosEmbarcaciones()
    {
        return $this->hasMany(UsosEmbarcaciones::className(), ['id_remero' => 'id_remero']);
    }

    /**
     * Gets query for [[MatriculaEmbarcacions]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMatriculaEmbarcacions()
    {
        return $this->hasMany(Embarcaciones::className(), ['matricula' => 'matricula_embarcacion'])->viaTable('usos_embarcaciones', ['id_remero' => 'id_remero']);
    }
}
