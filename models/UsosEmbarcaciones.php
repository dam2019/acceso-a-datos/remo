<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "usos_embarcaciones".
 *
 * @property int $id_uso
 * @property string|null $matricula_embarcacion
 * @property int|null $id_remero
 *
 * @property FechasUso[] $fechasUsos
 * @property Embarcaciones $matriculaEmbarcacion
 * @property Remeros $remero
 */
class UsosEmbarcaciones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'usos_embarcaciones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_remero'], 'integer'],
            [['matricula_embarcacion'], 'string', 'max' => 6],
            [['matricula_embarcacion', 'id_remero'], 'unique', 'targetAttribute' => ['matricula_embarcacion', 'id_remero']],
            [['matricula_embarcacion'], 'exist', 'skipOnError' => true, 'targetClass' => Embarcaciones::className(), 'targetAttribute' => ['matricula_embarcacion' => 'matricula']],
            [['id_remero'], 'exist', 'skipOnError' => true, 'targetClass' => Remeros::className(), 'targetAttribute' => ['id_remero' => 'id_remero']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_uso' => 'Id Uso',
            'matricula_embarcacion' => 'Matricula Embarcacion',
            'id_remero' => 'Id Remero',
        ];
    }

    /**
     * Gets query for [[FechasUsos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFechasUsos()
    {
        return $this->hasMany(FechasUso::className(), ['id_uso' => 'id_uso']);
    }

    /**
     * Gets query for [[MatriculaEmbarcacion]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMatriculaEmbarcacion()
    {
        return $this->hasOne(Embarcaciones::className(), ['matricula' => 'matricula_embarcacion']);
    }

    /**
     * Gets query for [[Remero]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRemero()
    {
        return $this->hasOne(Remeros::className(), ['id_remero' => 'id_remero']);
    }
}
