<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "embarcaciones".
 *
 * @property string $matricula
 * @property string $nombre_tecnico
 * @property string|null $mote
 * @property string|null $fabricante
 * @property int|null $num_tripulantes
 * @property int|null $necesita_patron
 * @property float|null $eslora
 *
 * @property JuegosRemos $juegosRemos
 * @property Patrocinios[] $patrocinios
 * @property Patrocinadores[] $codigoPatrocinadors
 * @property UsosEmbarcaciones[] $usosEmbarcaciones
 * @property Remeros[] $remeros
 */
class Embarcaciones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'embarcaciones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['matricula', 'nombre_tecnico'], 'required'],
            [['num_tripulantes', 'necesita_patron'], 'integer'],
            [['eslora'], 'number'],
            [['matricula'], 'string', 'max' => 6],
            [['nombre_tecnico'], 'string', 'max' => 12],
            [['mote', 'fabricante'], 'string', 'max' => 15],
            [['matricula'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'matricula' => 'Matricula',
            'nombre_tecnico' => 'Nombre Tecnico',
            'mote' => 'Mote',
            'fabricante' => 'Fabricante',
            'num_tripulantes' => 'Num Tripulantes',
            'necesita_patron' => 'Necesita Patron',
            'eslora' => 'Eslora',
        ];
    }

    /**
     * Gets query for [[JuegosRemos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJuegosRemos()
    {
        return $this->hasOne(JuegosRemos::className(), ['matricula_embarcacion' => 'matricula']);
    }

    /**
     * Gets query for [[Patrocinios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPatrocinios()
    {
        return $this->hasMany(Patrocinios::className(), ['matricula_embarcacion' => 'matricula']);
    }

    /**
     * Gets query for [[CodigoPatrocinadors]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoPatrocinadors()
    {
        return $this->hasMany(Patrocinadores::className(), ['codigo' => 'codigo_patrocinador'])->viaTable('patrocinios', ['matricula_embarcacion' => 'matricula']);
    }

    /**
     * Gets query for [[UsosEmbarcaciones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsosEmbarcaciones()
    {
        return $this->hasMany(UsosEmbarcaciones::className(), ['matricula_embarcacion' => 'matricula']);
    }

    /**
     * Gets query for [[Remeros]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRemeros()
    {
        return $this->hasMany(Remeros::className(), ['id_remero' => 'id_remero'])->viaTable('usos_embarcaciones', ['matricula_embarcacion' => 'matricula']);
    }
}
